
import pygame
import sys
from constants import *
from functions import *


def sudden_death(SNAKES,number_of_apples,end_score):

    speed_increase = 0
    respawn = False

    if(len(SNAKES)==1 and end_score == 1):
        rainbow = end_score
    else:
        rainbow = 0

    # create food variables
    max_p_apples = find_max_poison(number_of_apples)
    foods = generate_foods(number_of_apples)


    # main game loop
    while True:

        for event in pygame.event.get():
            # exit the game by quit button or esc key
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    return [ snake.score for snake in SNAKES ]


            # for each snake
            for i,snake in enumerate(SNAKES):
                # if the snake is still alive
                if snake.is_alive:
                    # if the key event matches the controls for the current snake
                    if event.type == pygame.KEYDOWN and event.key in INPUTS_TABLES[i]:
                        # change the snakes direction
                        snake.change_direction(event.key)


        # GFX
        screen.fill(BLACK)

        # for all snakes
        for snake in SNAKES:

            if snake.is_alive:
                # move_snake will check if the snake ate any foods, and return a new list of foods
                foods = snake.move_snake(foods)
                # check if food needs to be created
                if len(foods) < number_of_apples:
                    foods = add_food(foods,max_p_apples)
                # draw the snake
                for i,pos in enumerate(snake.body):
                    if rainbow:
                        body_color =  RAINBOW_COLORS[(i+rainbow)%len(RAINBOW_COLORS)]
                    else:
                        body_color = snake.color
                    pygame.draw.rect(screen,body_color, pygame.Rect(pos[0],pos[1],BLOCK_SIZE,BLOCK_SIZE))


        # draw the food pieces
        for (x,y), is_p in foods:
            if is_p:
                # food is poison
                pygame.draw.rect(screen, POISON_PURPLE, pygame.Rect(x,y,BLOCK_SIZE,BLOCK_SIZE))
            else:
                # food is not poison
                pygame.draw.rect(screen, RED, pygame.Rect(x,y,BLOCK_SIZE,BLOCK_SIZE))


        # draw player scores tokens
        for snake in SNAKES:
            score_token = FONT2.render(f"Player {snake.num + 1}: {snake.score}",True,snake.color)
            screen.blit(score_token,(((WINDOW_WIDTH//(len(SNAKES)+1))*(snake.num + 1))-50,30))




        # |----------------------|
        # | game over conditions |
        # |----------------------|

        # snakes die when hitting the edge of the screen
        for snake in SNAKES:
            if snake.is_alive:
                if snake.position[0] < 0 or snake.position[0] > WINDOW_WIDTH - BLOCK_SIZE or \
                snake.position[1] < 0 or snake.position[1] > WINDOW_HEIGHT - BLOCK_SIZE:
                    # kill the snake
                    snake.snake_died(respawn)



        # check if snakes have hit themselves
        for snake in SNAKES:
            if snake.is_alive:
                if snake.position in snake.body[1:]:

                    # turn the dead snake into food
                    foods = add_food(foods, max_p_apples, snake.body[1::2])
                    # kill the snake
                    snake.snake_died(respawn)



        # check if snake has hit any other snake
        for i,snake in enumerate(SNAKES):
            if snake.is_alive:
                # list of all alive snakes excluding the current snake
                other_snakes = [ snake for snake in SNAKES[:i] if snake.is_alive ] + [ snake for snake in SNAKES[i+1:] if snake.is_alive]
                for other_snake in other_snakes:
                    if snake.did_collide(other_snake):
                        # turn the dead snake into food
                        foods = add_food(foods, max_p_apples, snake.body[1::2])
                        # kill the snake
                        snake.snake_died(respawn)







        # check if any snakes are still alive
        # multiplayer should end when the second to last snake dies
        alive_snakes = [ snake.is_alive for snake in SNAKES ]

        if not any(alive_snakes):
                # if there are no more alive snakes
            return [ snake.score for snake in SNAKES ]

        elif (alive_snakes.count(True) < 2) and len(SNAKES) > 1:
                # or if these is one snake alive in multiplayer

            snake = SNAKES[alive_snakes.index(True)]
            scores = [snake.score for snake in SNAKES]
            scores.remove(snake.score)

                # if the score of the last snake alive is greater than all the other snakes scores
            if snake.score > max(scores):
                return [ snake.score for snake in SNAKES ]

        rainbow += 1
        pygame.display.update()
        speed_increase = speed_check(speed_increase,SNAKES)
        pygame.time.Clock().tick(BASE_FPS + speed_increase)
