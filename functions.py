
import pygame
import random
from constants import *




def speed_check(speed_increase,snakes):
    # adjust speed increase interval based on the size of the largest snake
    current_speed = max([len(snake.body) for snake in snakes]) // 3

    # the speed should only increase not decrease
    if current_speed > speed_increase:
        speed_increase = current_speed

    return speed_increase


def adjust_settings(number_of_players, snake_controls, snake_colors):
    if len(snake_colors) < number_of_players:
        # if we have added a player
        snake_colors.append(pick_color(0,snake_colors))
        snake_controls.append(True)
    elif len(snake_colors) > number_of_players:
        # we have removed a player
        snake_colors.pop()
        snake_controls.pop()
    return snake_controls,snake_colors


def draw_value_button(text, button_color, x, y, value):
    # Set button dimensions and colors
    button_height = 40
    text_color = (255, 255, 255)  # White

    # Calculate the width of the text
    label_text = f"{text}: "
    value_text = f"0{value}" if value < 10 else str(value)
    label = FONT.render(label_text, True, button_color)
    value = FONT.render(value_text, True, WHITE)
    label_width = label.get_width()
    string_width = label_width + value.get_width()

    # Draw the text
    text_x = x
    text_y = y + (button_height - label.get_height()) // 2
    screen.blit(label, (text_x, text_y))
    screen.blit(value, (text_x + label_width,text_y))

    # Draw the decrement button
    decrement_button_x = x + string_width + 10
    pygame.draw.rect(screen, GRAY, [decrement_button_x, y, 30, button_height])
    minus_label = FONT.render("-", True, text_color)
    screen.blit(minus_label, (decrement_button_x + 7, y + (button_height - minus_label.get_height()) // 2))

    # Draw the increment button
    increment_button_x = decrement_button_x + 35
    pygame.draw.rect(screen, GRAY, [increment_button_x, y, 30, button_height])
    plus_label = FONT.render("+", True, text_color)
    screen.blit(plus_label, (increment_button_x + 2, y + (button_height - plus_label.get_height()) // 2))


def draw_player_button(number,x,y,control,color):
    # generates two buttons
        # Player {num}:
            # pressing toggles the color of the player
        # control toggle
            # pressing toggles the control input method for the snake

    # create player name text
    player_label = FONT.render(f"Player {number+1}:",True, color)
    player_label_width = player_label.get_width()

    # draw the label
    screen.blit(player_label,(x,y))

    # toggle properties
    toggle_x = (x+player_label_width+30)
    toggle_y = y
    draw_toggle("LR","UDLR",control,toggle_x,toggle_y)




def draw_game_mode_button(x, y, game_mode):

    # Render the text
    label = FONT.render("Game Mode:", True, GREEN)
    label_width = label.get_width()

    # Draw the text on the screen
    screen.blit(label, (x, y))

    toggle_width = 100
    toggle_height = 40
    text_color = WHITE
    # Choose color based on the state
    toggle_color = ALL_COLORS[game_mode]

    # Draw the toggle
    pygame.draw.rect(screen, toggle_color, [x + label_width + 15, y, toggle_width, toggle_height])

    # Render the text
    game_mode_text = FONT.render(GAME_MODES[game_mode], True, text_color)

    # Draw the text on the toggle
    screen.blit(game_mode_text, (x + label_width + 15, y))



def draw_toggle(on_text,off_text,state,x,y):

    toggle_width = 100
    toggle_height = 40
    text_color = WHITE
    # Choose color based on the state
    toggle_color = BLUE if state else RED

    # Draw the toggle
    pygame.draw.rect(screen, toggle_color, [x, y, toggle_width, toggle_height])

    # Render the state text
    state_text = on_text if state else off_text
    state_label = FONT.render(state_text, True, text_color)

    # Draw the state text on the toggle
    screen.blit(state_label, (x, y))






def value_button_click(event, text, x, y, value, min_val, max_val):
    # Button dimensions
    button_height = 40
    text_width = FONT.render(f"{text}: 00", True, WHITE).get_width()

    # Calculating the positions of the decrement and increment buttons
    decrement_button_x = x + text_width + 10
    increment_button_x = decrement_button_x + 35

    # Check if the mouse click is within the decrement button area
    if (decrement_button_x <= event.pos[0] <= decrement_button_x + 30) and \
       (y <= event.pos[1] <= y + button_height):
        if value > min_val:
            value -= 1

    # Check if the mouse click is within the increment button area
    elif (increment_button_x <= event.pos[0] <= increment_button_x + 30) and \
         (y <= event.pos[1] <= y + button_height):
        if value < max_val:
            value += 1

    return value


def player_click(event,x,y,control,color,snake_colors):

    #take an event and an x,y and return click event values for player color and control
    toggle_x = x + 260
    toggle_y = y
    color = color_click(event,x,y,color,snake_colors)
    control = toggle_click(event,toggle_x,toggle_y,control)

    return control,color


def color_click(event,x,y,color,snake_colors):

    button_width = 225
    button_height = 50
    if (x <= event.pos[0] <= x + button_width) and (y <= event.pos[1] <= y + button_height):
        color = pick_color(color,snake_colors)
    return color


def pick_color(color,snake_colors):
    available_colors = [ i for i in range(len(ALL_COLORS)) if i not in snake_colors]
    larger_colors = [ c for c in available_colors if c > color ]
    if larger_colors:
        color = larger_colors.pop(0)
    else:
        color = available_colors.pop(0)
    return color



def toggle_click(event, x, y, state):
    # Toggle dimensions
    toggle_width = 125
    toggle_height = 50

    # Check if the mouse click is within the toggle area
    if (x <= event.pos[0] <= x + toggle_width) and (y <= event.pos[1] <= y + toggle_height):
        state = not state  # Toggle the state
    return state



def game_mode_button_click(event,x,y,game_mode):

    click_x = FONT.render("Game Mode:", True, GREEN).get_width() + x
    # Toggle dimensions
    toggle_width = 125
    toggle_height = 50

    # Check if the mouse click is within the toggle area
    if (click_x <= event.pos[0] <= click_x + toggle_width) and (y <= event.pos[1] <= y + toggle_height):
        game_mode = (game_mode + 1)% len(GAME_MODES)
    return game_mode




def generate_foods(number_of_apples):
        # create a list of foods
        #      pos      bool
        # [ ( [x, y], is_poison ), ... ]

        # game starts with all good apples "False"

    foods = [  food_item(game_start=True) for _ in range(number_of_apples) ]
    return foods




def food_item(num_of_p_apples=None,max_p_apples=None,pos=None,game_start=False):

    if game_start:
        is_p = False
    else:
        is_p = poison(num_of_p_apples,max_p_apples)

    if not pos:
        pos = [food_coordinate(WINDOW_WIDTH),food_coordinate(WINDOW_HEIGHT)]

    return (pos,is_p)



def food_coordinate(window_param):
    # return a random compatible value for a food coordinate
    return random.randrange(1,(window_param // BLOCK_SIZE)) * BLOCK_SIZE


def poison(num_of_p_apples, max_p_apples):
    # returns true or false on if the food is poison

    #  ->False if max_p_apples is 0
    if max_p_apples and num_of_p_apples < max_p_apples:
        return random.choice([True,False,False])
    else:
        return False


def add_food(foods,max_p_apples,snake_body=None):
    num_of_p_apples = [ is_p for _,is_p in foods ].count(True) if max_p_apples else 0

    if snake_body:
        # generate food tuples for each pos in snakebody
        snake_food = [ food_item(num_of_p_apples,max_p_apples,pos) for pos in snake_body]
        # snake_food = [ (pos, poison(num_of_p_apples,max_p_apples)) for pos in snake_body]
        foods.extend(snake_food)
        return foods
    else:
        foods.append(food_item(num_of_p_apples,max_p_apples))
        return foods


def ate_food(foods,food_pos,is_p):
    foods.remove((food_pos,is_p))
    return foods


def find_max_poison(number_of_apples):
    if number_of_apples == 1:
        return 0
    else:
        return max(number_of_apples//6, 1)
