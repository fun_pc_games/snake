# Snake

## Here is some code to run Snake on your machine.

## run the virtual environment
    source .venv/bin/activate

##  the first time you play you need to install requirements.txt
    pip install -r requirements.txt

## then you can just run the snake.py file
    python snake.py


## directions
-   select the number of players
-   click on the player to change snake color
-   click on the control toggle to change the control method for each player
-   click on the game mode to change game modes
    -   Sudden Death -> one life, high score
    -   King Cobra -> King of the Hill, first player to get to the winning score without dying, unlimited respawns

### "LR" uses only the Left, and Right keys like on old cell phones.
### "UDLR" uses the full Up, Down, Left, Right key inputs,

for LR:
-     snake 1 ->  down, right
-     snake 2 ->  s, d
-     snake 3 ->  2, 3     -> on the keypad
-     snake 4 ->  v, b

for UDLR:
-     snake 1 ->  up, down, left, right
-     snake 2 ->  w, a, s, d
-     snake 3 ->  5, 1, 2, 3     -> on the keypad
-     snake 4 ->  f, c, v, b
