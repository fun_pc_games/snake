
from constants import ALL_COLORS, BLOCK_SIZE, DIRECTIONS_LR, DIRECTIONS_UDLR, INPUTS_TABLES, START_POSITIONS
from functions import ate_food



class Snake:

    def __init__(self,snake_num,control,color):

        self.num = snake_num
        self.color = ALL_COLORS[color]
        self.score = 0
        self.is_alive = True

        # control -> a variable identifying the input method
        self.control = control
        self.input_table = INPUTS_TABLES[snake_num]

        # # start_position
        self.start_position, self.start_body, self.start_direction = START_POSITIONS.copy()[snake_num]
        self.reset_snake()



    def move_snake(self,foods):
            # advances the head of the snake based on the snakes current direction
            # grows the snakes body when food is eaten

        if self.direction == "UP" or self.direction == "DOWN":
                # if snake is moving up or down, adjust th y coordinate
            self.position[1] += {"UP": BLOCK_SIZE*-1, "DOWN": BLOCK_SIZE}[self.direction]

        elif self.direction == "LEFT" or self.direction == "RIGHT":
                # if snake is moving left or right, adjust the x coordinate
            self.position[0] += {"LEFT": BLOCK_SIZE*-1, "RIGHT": BLOCK_SIZE}[self.direction]

            # add the new position to the head of the snakes body
        self.body.insert(0,list(self.position))


        # check if snake ate food
        ate_good_food = False
        for food_pos,is_p in foods:
            if self.position == food_pos:
                foods = ate_food(foods,food_pos,is_p)
                if is_p:
                    # food is poison
                    self.score = max(0,(self.score-1))
                    if len(self.body) > 4:
                        self.body.pop()
                else:
                    # food is not poison
                    ate_good_food = True
                    self.score += 1

        if not ate_good_food:
            self.body.pop()

        return foods



    # change snake's direction
    def change_direction(self,key):
        # a method that takes the current key press and gives the snake a new direction value
        if self.control:
            # player is using LR controls
            self.direction = DIRECTIONS_LR[self.direction][self.input_table.index(key)]
        else:
            # player is using UDLR controls
            self.direction = DIRECTIONS_UDLR[self.direction][self.input_table.index(key)]


    def did_collide(self,other_snake) -> bool:
        # takes in another snake object and returns true if the snakes hit each other
        # snakes heads are the same position or
        # snakes heads were adjacent, and crossed into each other at the same time
        head_on = (self.position == other_snake.body[1] and other_snake.position == self.body[1]) or self.position == other_snake.position
        if head_on:
            return len(self.body) < len(other_snake.body)
        else:
            return self.position in other_snake.body


    def snake_died(self,respawn):
        if respawn:
            # respawn is on in this game mode
            #  game_mode is King Cobra
            self.reset_snake()
            self.score = 0
            # ^^^^ comment this line out for cummulative points
        else:
            self.is_alive = False


    def reset_snake(self):
        self.position = self.start_position.copy()
        self.body = self.start_body.copy()
        self.direction = self.start_direction
