# |---------------------------------|
# |       PYTHON_SNAKE_ARCADE       |
# |---------------------------------|

# version 3.0

"""
# |-----------------------------------|
# |        new features to add        |
# |-----------------------------------|
"""


import pygame
from constants import *
from functions import *
from intro import intro
from game_over import game_over
from snake_class import Snake
from sudden_death import sudden_death
from king_cobra import king_cobra
from team_snake import team_snake



pygame.init()



# |---------------------------------|
# |            game loop            |
# |---------------------------------|

    # game modes

GAME_MODE_FUNCTIONS = [
    sudden_death,
    king_cobra,
    team_snake
]

number_of_players = 1
number_of_apples = 100
snake_controls = [True]*number_of_players
snake_colors = [_ for _ in range(number_of_players)]
game_mode = 0
end_score = 1



while 1:

    game_mode,end_score,number_of_players,number_of_apples,snake_controls,snake_colors = intro(game_mode,end_score,number_of_players,number_of_apples,snake_controls,snake_colors)

    SNAKES = [Snake(num,snake_controls[num],snake_colors[num]) for num in range(number_of_players)]

    scores = GAME_MODE_FUNCTIONS[game_mode](SNAKES,number_of_apples,end_score)

    game_over(scores,snake_colors,game_mode)
