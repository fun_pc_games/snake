import pygame

pygame.init()



# |----------------------|
# |        Intro         |
# |        Screen        |
# |----------------------|



GAME_MODES = [
    "Sudden Death",
    "King Cobra",
    "Team Snake"
]




# colors
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
PURPLE = (75, 0, 130)
ORANGE = (255,127,0)
VIOLET = (148,0,211)
YELLOW = (255, 255, 0)
PINK = (255, 0, 127)
TEAL = (0, 255, 255)
GRAY = (70, 70, 70)
ALL_COLORS = [GREEN,BLUE,PURPLE,PINK,TEAL,ORANGE,YELLOW,WHITE]

BLACK = (0, 0, 0)
RED = (255, 0, 0)
POISON_PURPLE = (145, 70, 145)

    # window parameters
BLOCK_SIZE = 30
WINDOW_WIDTH = BLOCK_SIZE * 60
WINDOW_HEIGHT = BLOCK_SIZE * 30
START_Y = BLOCK_SIZE * 5
START_X = START_Y
BASE_FPS = 5
FONT = pygame.font.SysFont('Noto Sans', 80)
FONT2 = pygame.font.SysFont('Noto Sans Mono', 30)


    # snake properties
START_POSITIONS = [
    #   pos      body
    # [x, y], [[],[],[]] , direction
    [[START_X,START_Y],[[START_X,START_Y],[START_X - BLOCK_SIZE,START_Y],[START_X - BLOCK_SIZE*2,START_Y]],"RIGHT"],
    [[START_X,WINDOW_HEIGHT - START_Y], [[START_X,WINDOW_HEIGHT - START_Y],[START_X,WINDOW_HEIGHT - START_Y + BLOCK_SIZE],[START_X,WINDOW_HEIGHT - START_Y + BLOCK_SIZE*2]], "UP"],
    [[WINDOW_WIDTH - START_X,START_Y], [[WINDOW_WIDTH - START_X,START_Y],[WINDOW_WIDTH - START_X, START_Y - BLOCK_SIZE],[WINDOW_WIDTH - START_X,START_Y - BLOCK_SIZE*2]], "DOWN"],
    [[WINDOW_WIDTH - START_X,WINDOW_HEIGHT - START_Y], [[WINDOW_WIDTH - START_X,WINDOW_HEIGHT - START_Y],[WINDOW_WIDTH - START_X + BLOCK_SIZE,WINDOW_HEIGHT - START_Y],[WINDOW_WIDTH - START_X + BLOCK_SIZE*2,WINDOW_HEIGHT - START_Y]],"LEFT"]
]

# control True
# control map for LR input method
DIRECTIONS_LR = {
    # "direction": [ "up", "left",'down',"right"]
    "UP" : ["UP", "UP", "LEFT", "RIGHT"],
    "LEFT" : ["LEFT", "LEFT", "DOWN", "UP"],
    "DOWN" : ["DOWN", "DOWN", "RIGHT", "LEFT"],
    "RIGHT" : ["RIGHT", "RIGHT", "UP", "DOWN"]
}

# control False
# control map for UDLR input method
DIRECTIONS_UDLR = {
    # "direction": [ "up", "left",'down',"right"]
    "UP" : ["UP", "LEFT", "UP", "RIGHT"],
    "LEFT" : ["UP", "LEFT", "DOWN", "LEFT"],
    "DOWN" : ["DOWN", "LEFT", "DOWN", "RIGHT"],
    "RIGHT" : ["UP", "RIGHT", "DOWN", "RIGHT"]
}


# control keys for each snake
INPUTS_TABLES= [
    # snake 1 ->  up, down, left, right
    [pygame.K_UP,pygame.K_LEFT,pygame.K_DOWN,pygame.K_RIGHT],
    # snake 2 ->  5, 1, 2, 3
    [pygame.K_KP5,pygame.K_KP1,pygame.K_KP2,pygame.K_KP3],
    # snake 3 ->  w, a, s, d
    [pygame.K_w,pygame.K_a,pygame.K_s,pygame.K_d],
    # snake 4 ->  f, c, v, b
    [pygame.K_f,pygame.K_c,pygame.K_v,pygame.K_b],
]

RAINBOW_COLORS = [GREEN,BLUE,PURPLE,VIOLET,RED,ORANGE,YELLOW]

# Initialize the window
screen = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption('S_n_a_k_e !!')
