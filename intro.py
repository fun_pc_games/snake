
import pygame
import sys
from constants import *
from functions import *


# |----------------------------|
# |        info screen         |
# |----------------------------|


# new features / bug-fix


def intro(game_mode,end_score,number_of_players,number_of_apples,snake_controls,snake_colors):

    x_start = 100
    running = True

        # play button properties
    play_rect = pygame.Rect((WINDOW_WIDTH //2) + 100 , (WINDOW_HEIGHT//2)+50, 400, 150)
    play_color = GREEN
    play_text_color = GRAY
    play_font = pygame.font.SysFont('Noto Sans', 100)

        # play Button text
    play_surface = play_font.render('Snake!', True, play_text_color)
    play_text_rect = play_surface.get_rect(center=play_rect.center)


    game_mode_x = (WINDOW_WIDTH //3)+90
    game_mode_y = (WINDOW_HEIGHT//3)

    while running:

        screen.fill(BLACK)

        # Draw buttons and texts
        draw_value_button("Number of Players", GREEN, x_start, 50, number_of_players)
        draw_value_button("Number of Apples", RED, x_start, 150, number_of_apples)

        for num in range(number_of_players):
            draw_player_button(num, x_start,(250 + num*100),snake_controls[num],ALL_COLORS[snake_colors[num]])

        # draw play button!
        pygame.draw.rect(screen, play_color, play_rect)
        screen.blit(play_surface, play_text_rect)

        # draw game mode toggle
        draw_game_mode_button(game_mode_x,game_mode_y,game_mode)
        if game_mode == 1 or game_mode == 2:
            draw_value_button("Points For Win", BLUE,game_mode_x,game_mode_y+100,end_score)

        # watch for user input
        for event in pygame.event.get():

            # exit on exit click or escape key
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                running = False
                pygame.quit()
                sys.exit()


            elif event.type == pygame.MOUSEBUTTONDOWN:
                # Check if buttons are clicked and update values

                number_of_players = value_button_click(event, "Number of Players", x_start, 50, number_of_players, 1, 4)
                snake_controls,snake_colors = adjust_settings(number_of_players,snake_controls,snake_colors)
                number_of_apples = value_button_click(event, "Number of Apples", x_start, 150, number_of_apples, 1, 100)

                game_mode = game_mode_button_click(event,game_mode_x, game_mode_y,game_mode)
                end_score = value_button_click(event,"Points For Win",game_mode_x, game_mode_y+100,end_score,1,100)

                for i in range(number_of_players):
                    snake_controls[i],snake_colors[i] = player_click(event, x_start, 250 + i * 100,snake_controls[i],snake_colors[i],snake_colors)

                # if play button pressed exit function with current settings
                if play_rect.collidepoint(event.pos):
                    return game_mode, end_score, number_of_players, number_of_apples, snake_controls,snake_colors


            # if space pressed exit function with current settings
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                    return  game_mode, end_score, number_of_players, number_of_apples, snake_controls,snake_colors

        pygame.display.update()
