
import pygame
import sys
from constants import *
from functions import *



# |-----------------------------------|
# |          game over screen         |
# |-----------------------------------|



def game_over(scores,snake_colors,game_mode):

        # get winning score
    winner_score = max(scores)

        # Define the high score file path
    high_score_file_path = 'high_score.txt'

        # Try to read the existing high score, if the file doesn't exist, create it with a score of 0
    try:
        with open(high_score_file_path, 'r') as file:
            high_score = int(file.read())
    except FileNotFoundError:
        with open(high_score_file_path, 'w') as file:
            file.write('0')
        high_score = 0

        # Check if the current score is higher than the high score
    if winner_score > high_score:
        high_score = winner_score
        with open(high_score_file_path, 'w') as file:
            file.write(str(high_score))

        # Define Play Again button properties
    play_again_rect = pygame.Rect(WINDOW_WIDTH // 2 - 150, WINDOW_HEIGHT - 150, 300, 50)
    play_again_color = GREEN
    play_again_text_color = GRAY
    play_again_font = pygame.font.SysFont('Noto Sans', 40)

        # play again Button text
    play_again_surface = play_again_font.render('Snake Again!', True, play_again_text_color)
    play_again_text_rect = play_again_surface.get_rect(center=play_again_rect.center)

        # Define scores properties
    score_font = pygame.font.SysFont('Noto Sans', 60)
    score_surfaces = []
    for i,score in enumerate(scores):
        score_surfaces.append({"surface":score_font.render(f'player {i+1}: {str(score)}', True, ALL_COLORS[snake_colors[i]]),"score":score})
    score_surfaces.sort(key=lambda x: x["score"], reverse=True)
    # score_surface = score_font.render('Winner: ' + str(winner_score), True, SNAKE_COLORS[scores.index(winner_score)])
    high_score_surface = score_font.render('High Score: ' + str(high_score), True, WHITE)

    scores_rect = score_surfaces[0]["surface"].get_rect(center=(WINDOW_WIDTH // 2, 100))
    high_score_rect = high_score_surface.get_rect(center=(WINDOW_WIDTH // 2, WINDOW_HEIGHT - 200))

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if play_again_rect.collidepoint(event.pos):
                    return  # Exit the function to restart the game
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    return

        # Draw the Play Again button and score texts
        screen.fill(BLACK)
        for i,surface in enumerate(score_surfaces):
            screen.blit(surface["surface"], (scores_rect[0],scores_rect[1]+ (100*i)))
        screen.blit(high_score_surface, high_score_rect)
        pygame.draw.rect(screen, play_again_color, play_again_rect)
        screen.blit(play_again_surface, play_again_text_rect)

        pygame.display.flip()
        pygame.time.wait(100)
